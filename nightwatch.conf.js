var selenium = require('selenium-server-standalone-jar');
var chromeDriver = require('chrome-driver-standalone');

var config = {
  "src_folders" : ["test"],
  "output_folder" : "reports",
  "custom_commands_path" : "",
  "custom_assertions_path" : "",
  "page_objects_path" : "",
  "globals_path" : "",

  "selenium" : {
	"start_process" : true,
	"server_path" : selenium.path,
	"host":"127.0.0.1",
	"port" : 4444,
	"cli_args" : {
	  "webdriver.chrome.driver" : chromeDriver.path
	}
  },

  "test_settings" : {
	"default" : {
	  "launch_url" : "http://yahoo.com.tw",

	  "selenium_port"  : 4444,
	  "selenium_host"  : "localhost",
	  "silent": true,
	  "screenshots" : {
		  "enabled" : true,
		  "path" : "reports/screenshots"
	  },
	  "desiredCapabilities": {
		"browserName": "chrome",
		"chromeOptions" : {
		  "args" : ["start-maximized"]
		}
	  }
	}

  }
}

module.exports = (function(settings){
	settings.test_workers = false;
	return settings;
})(config);
