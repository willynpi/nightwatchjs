Automatic Web Testing

## Environment Requirement ##

1. Node.js
2. JDK
3. webdrivers depends on OS

## Run Command ##

Unzip files and run project by command lines.

### install modules ###


```
#!command line

$ npm install
```


### run tests ###

```
#!command line

$ node nightwatch.js [tests dir]
```

### generate reports ###

```
#!command line

$ node nightwatch-html-reporter -d [reports dir]
```