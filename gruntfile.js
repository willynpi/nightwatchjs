module.exports = function(grunt) {
	grunt.initConfig({
		nightwatch: {
			options: {
				standalone: true,
				// jar_version: '2.44.0',
				// jar_path: '/opt/selenium/server.jar',
				// jar_url: 'http://domain.com/files/selenium-server-standalone-1.2.3.jar',
				globals: { },
		        globals_path: '',
		        custom_commands_path: '',
		        custom_assertions_path: '',
		        src_folders: ['test'],
		        output_folder: 'report',
		        test_settings: {},
		        selenium: {}
			},
			custom: {
		        // custom target + overrides 
		        config_path: '/path/to/file.json',
		        src_folders: ['other_tests/nightwatch']
		    }
		}
	});

	grunt.loadNpmTasks('grunt-nightwatch');
};